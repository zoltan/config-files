---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gdegug = require("gears.debug")
local gfs = require("gears.filesystem")
local themes_path = gfs.get_configuration_dir().."themes/"

local theme = {}

-- gfs.get_configuration_dir()..
dark_blue = "#041424"
orange = "#EB4E2F"
bright_blue = "#2CC3B4"
light_blue = "#3D6465"
light_gray = "#7C8494"


theme.font          = "sans 12"

theme.bg_normal     = dark_blue
theme.bg_focus      = light_blue
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#ffffff"
theme.fg_focus      = dark_blue
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = dpi(20)
theme.border_width  = dpi(5)
theme.border_normal = light_blue
theme.border_focus  = orange
theme.border_marked = "#91231c"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."zedka/submenu.png"
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)

local gears = require("gears")

theme.tasklist_spacing = dpi(5)
theme.tasklist_bg_normal = theme.bg_normal
theme.tasklist_fg_normal = "#ffffff"
theme.tasklist_bg_focus = theme.bg_normal
theme.tasklist_fg_focus = "#ffffff"
theme.tasklist_bg_minimize = theme.border_normal
theme.tasklist_fg_minimize = "#ffffff"

theme.tasklist_shape = gears.shape.rectangle
theme.tasklist_shape_border_width = dpi(5)
theme.tasklist_shape_border_color = light_blue
theme.tasklist_shape_border_color = theme.bg_focus
theme.tasklist_shape_border_width_focus = dpi(5)
theme.tasklist_shape_border_color_focus = bright_blue

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = themes_path.."zedka/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = themes_path.."zedka/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = themes_path.."zedka/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path.."zedka/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = themes_path.."zedka/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."zedka/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."zedka/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = themes_path.."zedka/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path.."zedka/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."zedka/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path.."zedka/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = themes_path.."zedka/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."zedka/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."zedka/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."zedka/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."zedka/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."zedka/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."zedka/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."zedka/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = themes_path.."zedka/titlebar/maximized_focus_active.png"

theme.wallpaper = themes_path.."zedka/background.jpg"

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."zedka/layouts/fairhw.png"
theme.layout_fairv = themes_path.."zedka/layouts/fairvw.png"
theme.layout_floating  = themes_path.."zedka/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."zedka/layouts/magnifierw.png"
theme.layout_max = themes_path.."zedka/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."zedka/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."zedka/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."zedka/layouts/tileleftw.png"
theme.layout_tile = themes_path.."zedka/layouts/tilew.png"
theme.layout_tiletop = themes_path.."zedka/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."zedka/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."zedka/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."zedka/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."zedka/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."zedka/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."zedka/layouts/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
