syntax on
filetype on
filetype plugin on
filetype indent on

map <F10> :TlistToggle<CR>
map <F11> :%s/\s\+$//g<CR>
map <F12> :!ctags -R --c-kinds=+p --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

if has("autocmd") && exists("+omnifunc")
	autocmd Filetype *
		\	if &omnifunc == "" |
		\		setlocal omnifunc=syntaxcomplete#Complete |
		\	endif
endif

set tags+=~/.vim/tags/cpp

set completeopt=longest,menuone,preview
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

set t_Co=256
"colorscheme xoria256
colorscheme wombat256

set number
set nocompatible
set backspace=indent,eol,start
set showmatch
set ruler
set showmode
set undolevels=200
set autoindent
set history=50
set wildmenu
set ttyfast
set incsearch
set mouse=a
set scrolloff=10
"set list
"set lcs=tab:»·
"set lcs+=trail:·

set cursorline
hi CursorLine term=none cterm=none ctermbg=0
hi CursorColumn term=none cterm=none ctermbg=0

highlight WhitespaceEOL ctermbg=red
match WhitespaceEOL /\s\+$/
autocmd WinEnter * match WhiteSpaceEOL /\s\+$/

set nomodeline
