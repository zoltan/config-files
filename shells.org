#+TITLE: Shells configuration
#+OPTIONS: ^:nil
#+STARTUP: content

* ZSH configuration
#+PROPERTY: header-args :tangle ~/.zshrc

Configuration get written to ~~/.zshrc~ by running ~org-babel-tangle~ (~C-c C-v C-t~).

Resources:
- ~zsh(1)~, ~zshall(1)~ and other zsh manuals
- http://zsh.sourceforge.net/Guide/zshguide.html

** ~ls~ tweaks

Some convenient aliases for ~ls~.

#+begin_src shell
alias l='ls -l'
alias la='ls -la'
alias lh='ls -lh'
#+end_src

Colorized output on FreeBSD and macos, see ~ls(1)~ for details on the colors.

#+begin_src shell
alias ls='ls --color'

export LSCOLORS=cxfxcxdxbxegedabagacad
#+end_src

** Aliases

I prefer these commands to be more verbose even if the output may slow things
down. Unalias command is accessible by prefixing it with a backslash
(eg. ~\cp~).

#+begin_src shell
alias cp='cp -v'
alias mv='mv -v'
alias rm='rm -v'
#+end_src

** Prompt

See _SIMPLE PROMPT ESCAPES_ in ~zshmisc(1)~.

- ~%m~, hostname up to the first ~.~.
- ~%3~~, current working directory, only the 3 trailing components are
  displayed.
- ~%(?..%? )~, shows the return status of the last command if it wasn't ~0~.
- ~%F{color}...%f~, changes the foreground color.
- ~%#~, a ~#~ if the shell runs with privileges, ~%~ otherwise.

#+begin_src shell
PS1="%m:%3~ %(?..%F{red}%?%f )%# "
export PS1
#+end_src

** Pager and man pages

Use ~less(1)~ as pager.

#+begin_src shell
export PAGER=less
#+end_src

*** Colorized man pages

The behavior of ~less~ can be modified to include colors in manpages using
variables ~LESS_TERMCAP_*~. I can't find any documentation about this in
~less(1)~ or ~lesskey(1)~ though.

The best explanation I could find is this [[https://unix.stackexchange.com/a/108840][post]] on _stackexchange_.

From ~termcap(5)~:
- ~md~: start bold mode
- ~me~: end bold mode
- ~us~: start underline
- ~ue~: end underline

#+begin_src shell
export LESS_TERMCAP_md=$(print -P %F{red}%B)
export LESS_TERMCAP_me=$(print -P %f%b)
export LESS_TERMCAP_us=$(print -P %F{green}%B)
export LESS_TERMCAP_ue=$(print -P %f%b)
#+end_src

The problem with including escape sequences in the environment is that it messes
up the output of ~env(1)~. This can be worked around by moving those variables
to the file ~~/.lesskey~.

** History

Parameters described in ~zshparam(1)~:

- ~SAVEHIST~, the maximum number of history events to save in the history file.
- ~HISTSIZE~, the maximum number of events stored in the internal history list.

#+begin_src shell
HISTFILE=~/.zhistory
HISTSIZE=1100
SAVEHIST=1000

setopt hist_expire_dups_first
setopt hist_ignore_dups
#+end_src

Option ~hist_ignore_space~ is pretty handy, it prevents commands lines starting
with a space to be added to the history file.

#+begin_src shell
setopt hist_ignore_space
#+end_src

Control over shared history, when history file is written etc can be controlled
with the option ~share_history~ in ~zshoptions(1)~.

Search history by prefixes by typing the beginning of a command and then using
up and down arrows to cycle through commands in history with the same
prefix. This is more powerful than using ~!prefix~. See ~history-search-end~ in
~zshcontrib(1)~.

#+begin_src shell
autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

bindkey "$terminfo[kcuu1]" history-beginning-search-backward-end
bindkey "$terminfo[kcud1]" history-beginning-search-forward-end
#+end_src

** ~cd~ tweaks

Makes ~cd~ push the old directory to the directory stack. The stack can also be
manipulated with ~pushd~ and ~popd~ while ~dirs~ will display the stack's
content.

~cd -n~ will move back to the Nth item (1 based) from the top of the stack
(eg. ~cd -1~ will move back to the previous working directory). ~cd +n~ will
change to the Nth item (0 based) from the bottom of the stack (see ~pushd_minus~
in ~zshoptions(1)~).

#+begin_src shell
alias dirs='dirs -v'
DIRSTACKSIZE=64

setopt auto_pushd
setopt pushd_ignore_dups
setopt pushd_minus
#+end_src

** Line edition

Use emacs keymap.

#+begin_src shell
bindkey -e
#+end_src

** Completion

*** New completion

~zsh~ has a "new" completion system since version 4 (which was released in
2001). This system is initialized with ~compinit~.

#+begin_src shell
autoload -U compinit
compinit
#+end_src

Allow selection of candidates using arrows.

#+begin_src shell
zstyle ':completion:*' menu select=1
#+end_src

*** Options

Option ~auto_list~ shows all candidates when first hitting ~<tab>~ and option
~auto_menu~ cycles through the possible completions by pressing ~<tab>~,
starting at the second time ~<tab>~ is pressed. Any unambiguous prefix is
completed on the first ~<tab>~ hit though (option ~list_ambiguous~).

Option ~menu_complete~, which overrides ~auto_menu~, makes that a candidate is
selected the first time ~<tab>~ is hit. I don't like that behavior so the option
is disabled.

#+begin_src shell
setopt auto_list
setopt auto_menu
setopt list_ambiguous
setopt list_types
setopt no_menu_complete
#+end_src

** Local configuration

Finally, add any local configuration to a file ~~/.zshrc.local~.

#+begin_src shell
  if [[ -r ~/.zshrc.local ]]
  then
      . ~/.zshrc.local
  fi
#+end_src
